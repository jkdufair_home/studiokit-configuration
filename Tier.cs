﻿namespace StudioKit.Configuration
{
	public static class Tier
	{
		public const string Local = "LOCAL";

		public const string Dev = "DEV";

		public const string Prod = "PROD";
	}
}