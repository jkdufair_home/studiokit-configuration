﻿namespace StudioKit.Configuration
{
	public static class RunConfiguration
	{
		public const string Debug = "DEBUG";

		public const string LoadTest = "LOADTEST";

		public const string Release = "RELEASE";
	}
}