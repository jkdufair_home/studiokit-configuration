﻿namespace StudioKit.Configuration
{
	public static class BaseAppSetting
	{
		public const string Tier = "TIER";

		public const string RunConfiguration = "RUN_CONFIGURATION";

		public const string SentryDsn = "SentryDSN";

		public const string AppInsightsInstrumentationKey = "APPINSIGHTS_INSTRUMENTATIONKEY";

		public const string WebsiteLoadCertificates = "WEBSITE_LOAD_CERTIFICATES";

		public const string SuperAdminEmailAddresses = "SuperAdminEmailAddresses";
	}
}